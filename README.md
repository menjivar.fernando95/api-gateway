# Api-gateway
Balanceo de carga y redireccionador

## Features

- mapeo de apis de los microservicios


## Installation
Importar el proyecto con intellijidea y aplicar correr el comando mvn install
Install the dependencies and devDependencies and start the server.

```sh
mvn install
```

Start...
Se recomienda tener iniciado el componente discovery-server antes de iniciar este componente
Se puede correr desde el iDE o ejecutar el siguiente comando abriendo una consola en la carpeta target donde se guarda el compilado

```sh
java -jar api-gateway-0.0.1-SNAPSHOT.jar
```
### Se debe tener instalado java 11, y  se puede ver la instancia del microservicio corriendo en el discovery server http://localhost:8761/

## License

MIT

**Free Software, Hell Yeah!**

